import * as React from 'react'

import CreditCard from './CreditCard'
import './style.app.scss'

const App = () => (
  <div className="application">
    <CreditCard />
  </div>
)

export default App
