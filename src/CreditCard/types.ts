export type Card = {
  brand: string
  name: string
  month: number
  year: number
  country: string
  zip: string
  numberComplete: boolean
  cvcComplete: boolean
}
