import * as React from 'react'
import { useStripe, useElements, CardNumberElement, CardCvcElement } from '@stripe/react-stripe-js'

import { Card } from './types'
import './styles.credit-card-form.scss'

type Props = {
  card: Card
  updateCard: (key: string, value: any) => void
}

const CARD_OPTIONS = {
  style: {
    base: {
      paddingLeft: 15,
      paddingRight: 15,
      paddingTop: 5,
      paddingBottom: 5,
      height: '50px',
      fontSize: '18px',
    },
    input: {
      borderRadis: '5px',
      border: '1px solid #ced6e0',
    },
  },
}

const CreditCardForm = ({ card, updateCard }: Props) => {
  const elements = useElements()
  const stripe = useStripe()

  return (
    <div className="credit-card-form">
      <label>
        Card Number
        <CardNumberElement id="number" options={CARD_OPTIONS} onChange={e => {
          updateCard('brand', e.brand)
          updateCard('numberComplete', e.complete)
        }} />
      </label>
      <label>
        Card Holder
        <input id="name" name="name" onChange={e => updateCard('name', e.target.value)} />
      </label>
      <label>
        CVV
        <CardCvcElement id="cvc" onChange={e => updateCard('cvcComplete', e.complete)} />
      </label>
      <label>
        Zip Code
        <input id="zip" name="zip" onChange={e => updateCard('zip', e.target.value)} />
      </label>
    </div>
  )
}

export default CreditCardForm
