import * as React from 'react'
import { loadStripe } from '@stripe/stripe-js'
import { Elements, ElementsConsumer } from '@stripe/react-stripe-js'

import CreditCardView from './CreditCardView'
import CreditCardForm from './CreditCardForm'
import './styles.credit-card.scss'

const { useState } = React
const stripePromise = loadStripe('pk_test_6pRNASCoBOKtIshFeQd4XMUh')

const CreditCard = () => {
  const [card, setCard] = useState(null)

  const updateCard = (key: string, value: any) => setCard({ ...card, [key]: value })

  return (
    <div className="credit-card">
      <Elements stripe={stripePromise}>
        <CreditCardView card={card} />
        <CreditCardForm card={card} updateCard={updateCard} />
      </Elements>
    </div>
  )
}

export default CreditCard
